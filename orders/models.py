# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Spacing model
class Spacing(models.Model):
    name = models.CharField(max_len=10)
    description = models.CharField(max_len=100)

    class Meta:
        verbose_name = 'spacing'

    def __str__(self):
        return self.name


# Academic levels model
class Format(models.Model):
    name = models.CharField(max_len=20)
    description = models.CharField(max_len=200)

    class Meta:
        verbose_name = 'format'
        verbose_name_plural = 'formats'

    def __str__(self):
        return self.name


# Order status model
class OrderStatus(models.Model):
    status = models.CharField(max_len=20)
    description = models.CharField(max_len=256)

    class Meta:
        verbose_name = 'Order status'
        verbose_name_plural = 'orders status'

    def __str__(self):
        return self.status


# Academic levels model
class AcademicLevel(models.Model):
    level = models.CharField(max_len=20)
    description = models.CharField(max_len=256)

    class Meta:
        verbose_name = 'academic level'
        verbose_name_plural = 'academic levels'

    def __str__(self):
        return self.level


# Discipline category
class DisciplineCategory(models.Model):
    name = models.CharField(max_len=200)

    class Meta:
        verbose_name = 'discipline category'
        verbose_name_plural = 'discipline categories'

    def __str__(self):
        return self.name


# Discipline model
class Discipline(models.Model):
    name = models.CharField(max_len=30)
    category = models.ForeignKey(DisciplineCategory, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'discipline'
        verbose_name_plural = 'disciplines'

    def __str__(self):
        return self.name


# WorkType model
class WorkType(models.Model):
    name = models.CharField(max_len=20)
    description = models.CharField(max_len=200)

    class Meta:
        verbose_name = 'work type'
        verbose_name_plural = 'work types'

    def __str__(self):
        return self.name


# class type models
class ServiceType(models.Model):
    service = models.CharField(max_len=20)
    description = models.CharField(max_len=200)

    class Meta:
        verbose_name = 'service type'
        verbose_name_plural = 'service types'

    def __str__(self):
        return self.service


# client models
class Client(models.Model):
    first_name = models.CharField(max_len=50)
    middle_name = models.CharField(max_len=50)
    last_name = models.CharField(max_len=50)
    email = models.EmailField(primary_key=True)

    class Meta:
        verbose_name = 'client'
        verbose_name_plural = 'clients'

    def __str__(self):
        return self. first_name + " " + self.last_name


# order model
class Order(models.Model):
    order_number = models.CharField(len=10, primary_key=True)
    title = models.CharField(max_length=100, default='Writer\'s Choice')
    sources = models.IntegerField()
    digital_sources = models.BooleanField()
    pages = models.IntegerField()
    words = models.IntegerField()
    slides = models.IntegerField()
    charts = models.IntegerField()
    details = models.CharField(max_len=1000)
    fee = models.DecimalField(8, 2)
    deadline = models.DateTimeField()
    time_placed = models.DateTimeField()
    support_comments = models.CharField(len=500)
    service_type = models.ForeignKey(ServiceType, on_delete=models.CASCADE)
    work_type = models.ForeignKey(WorkType, on_delete=models.CASCADE)
    academic_level = models.ForeignKey(AcademicLevel, on_delete=models.CASCADE)
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    format = models.ForeignKey(Format, on_delete=models.CASCADE)
    spacing = models.ForeignKey(Spacing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    status = models.ForeignKey(OrderStatus, on_delete=models.CASCADE)

    class Meta:
        ordering = ['order_number']
        verbose_name = 'order'
        verbose_name_plural = 'orders'

    def __str__(self):
        return self.order_number
